Name:           libevdev
Version:        1.13.3
Release:        1
Summary:        Library for handling evdev kernel devices
License:        MIT
URL:            http://www.freedesktop.org/wiki/Software/libevdev
Source0:        http://www.freedesktop.org/software/%{name}/%{name}-%{version}.tar.xz

BuildRequires:  libtool make gcc python3-devel check-devel

%description
Library for handling evdev kernel devices. It abstracts the ioctls
through type-safe interfaces and provides functions to change the
appearance of the device.

%package devel
Summary:        Development Package
Requires:       %{name} = %{version}-%{release}

%description devel
Development Package for %{name}.

%package utils
Summary:        Utilities Package
Requires:       %{name} = %{version}-%{release}

%description utils
Utilities to handle and/or debug evdev devices for %{name}.

%package help
Summary:        help file

%description help
#%%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -ivf
%configure --disable-silent-rules --disable-gcov

make %{?_smp_mflags}

%install
%make_install

%ldconfig_scriptlets

%check
make check

%files
%doc COPYING
%{_libdir}/libevdev.so.*
%exclude %{_libdir}/libevdev.a
%exclude %{_libdir}/libevdev.la

%files devel
%{_includedir}/libevdev-1.0/libevdev/libevdev.h
%{_includedir}/libevdev-1.0/libevdev/libevdev-uinput.h
%{_libdir}/libevdev.so
%{_libdir}/pkgconfig/libevdev.pc

%files utils
%{_bindir}/touchpad-edge-detector
%{_bindir}/mouse-dpi-tool
%{_bindir}/libevdev-tweak-device

%files help
%{_mandir}/man3/libevdev.3*
%{_mandir}/man1/libevdev-tweak-device.1*
%{_mandir}/man1/mouse-dpi-tool.1.gz
%{_mandir}/man1/touchpad-edge-detector.1*

%changelog
* Fri Nov 01 2024 maqi <maqi@uniontech.com> - 1.13.3-1
- Update to 1.13.3

* Wed Nov 01 2023 haomimi <haomimi@uniontech.com> - 1.13.1-1
- Update to 1.13.1

* Fri Oct 14 2022 Liu Zixian <liuzixian4@huawei.com> - 1.13.0-1
- Update to 1.13.0 and enable regression check

* Mon Apr 18 2022 Liu Zixian <liuzixian4@huawei.com> - 1.12.1-2
- fix rpm build

* Sun Apr 17 2022 Liu Zixian <liuzixian4@huawei.com> - 1.12.1-1
- Update to 1.12.1

* Wed Dec 1 2021 zhouwenpei <zhouwenpei1@huawei.com> - 1.11.0-2
- fix license in src and in spec are not same

* Tue Nov 30 2021 zhouwenpei <zhouwenpei1@huawei.com> - 1.11.0-1
- update to 1.11.0

* Sat Jan 30 2021 xinghe <xinghe1@huawei.com> - 1.10.1-1
- update to 1.10.1

* Thu Jul 23 2020 jinzhimin <jinzhimin2@huawei.com> - 1.8.0-2
- delete old tar file

* Thu Jul 16 2020 dingyue <dingyue5@openeuler.org> - 1.8.0-1
- Package update

* Tue Aug 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.5.9-6
- Package init
